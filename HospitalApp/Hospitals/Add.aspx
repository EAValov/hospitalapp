﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="HospitalApp.Hospitals.Add" MasterPageFile="~/Layout.Master" %>

<asp:Content runat="server" ContentPlaceHolderID ="head"> 
    <script src="../Scripts/AddHospital.js" type="text/javascript"></script>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="title">Add new Hospital</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID ="main">   

    <form id="addform" runat="server">
        <table id="dataTable">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Doctor</th>
                    <th></th>

                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <input type="text" name="Name" value="" /></td>
                    <td>
                        <select id="Doctor" name="Doctor"></select>
                    </td>
                </tr>
            </tbody>
        </table>
        <div>
            <button id="AddNewHospitalButton" type="submit">Submit</button>
        </div>
    </form>
    <button id="CancelNewHospital" class="float-left submit-button">Cancel and return to list</button>

    <script type="text/javascript">
        document.getElementById("CancelNewHospital").onclick = function () {
            location.href = "../Hospitals/List.aspx";
        };
    </script>
    </asp:Content>
