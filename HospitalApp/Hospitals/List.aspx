﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" MasterPageFile="~/Layout.Master" %>

    <asp:Content runat="server" ContentPlaceHolderID ="head">   
    <script src="../Scripts/HospitalList.js" type="text/javascript"></script>
    <script type="text/template" id="rowTemplate">
        <tr>
            <td>{HospitalID}</td>
            <td>{Name}</td>
            <td>{Doctor}</td>
            <td>
                <button data-id="{HospitalID}" data-action="details">Details</button>
            </td>
        </tr>
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="title">Hospitals List</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID ="main">   
    <form id="form1" runat="server">
        <table id="dataTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Doctor</th>
                    <th></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </form>

    <button id="AddHospitalButton" class="float-left submit-button">Add New Hospital!</button>

    <script type="text/javascript">
        document.getElementById("AddHospitalButton").onclick = function () {
            location.href = "../Hospitals/Add.aspx";
        };
    </script>

    </asp:Content>
    

