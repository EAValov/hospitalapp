﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HospitalApp.Hospitals
{
    public partial class Edit : System.Web.UI.Page
    {
        protected int id
        {
            get
            {
                int page;
                page = int.TryParse(Request.QueryString["id"], out page) ? page : 0;
                return page;
            }
        }
        protected string Name
        {
            get
            {
                string Name;
                Name = Request.QueryString["name"];
                return Name;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}