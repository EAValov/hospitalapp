﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="HospitalApp.Hospitals.Edit" MasterPageFile="~/Layout.Master" %>

<asp:Content runat="server" ContentPlaceHolderID ="head">  
    <script type="text/template" id="rowTemplate">
        <tr>
            <td>{HospitalID}</td>
            <td>
                <input type="text" name="Name" value="{Name}" />
            </td>
            <td>
                <select id="Doctor" name="Doctor"></select>
            </td>
            <td>
                <button data-id="{HospitalID}" data-action="update">Update</button>
                <button data-id="{HospitalID}" data-action="delete">Delete</button>
            </td>
        </tr>
    </script>
    </asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="title">Hospital Edit</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID ="main">   

    <form id="form1" runat="server">
        <table id="dataTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Doctor</th>
                    <th></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </form>
    <button id="CancelNewHospital" class="float-left submit-button">Cancel and return to list</button>

    <script type="text/javascript">
        document.getElementById("CancelNewHospital").onclick = function () {
            location.href = "../Hospitals/List.aspx";
        };

        (function () {
            String.prototype.format = function (dataObject) {
                return this.replace(/{(.+)}/g, function (match, propertyName) {
                    return dataObject[propertyName];
                });
            };
            function getData() {
                var myID = '<%=id%>'; //this is bad?
                if (myID != 0) {
                    $.getJSON("/Handlers/HospitalHandler.ashx?id=" + myID, null, displayData);
                } else {
                    location.href = "../Hospitals/List.aspx";
                }
            };
            function displayData(data) {
                var target = $("#dataTable tbody");
                target.empty();
                var template = $("#rowTemplate");
                data.forEach(function (dataObject) {
                    target.append(template.html().format(dataObject));
                });

                $('select#Doctor').each(function (index, value) {
                    Options((data[0].DoctorID - 1), value, 'FirstName', 'LastName');
                });

                $(target).find("button").click(function (e) {
                    $("*.errorMsg").remove();
                    $("*.error").removeClass("error");
                    var index = $(e.target).attr("data-id");
                    if ($(e.target).attr("data-action") == "delete") {
                        deleteData(index);
                    } else if ($(e.target).attr("data-action") == "details") {
                        detailView(index);
                    } else {
                        var hospitalData = { HospitalID: index };
                        var DoctorName = $('#Doctor').find(":selected").text();
                        $(e.target).closest('tr').find('input')
                        .each(function (index, inputElem) {
                            hospitalData[inputElem.name] = inputElem.value;
                        });
                        GetDoctorID(DoctorName, function (name) {
                            hospitalData['DoctorID'] = name;
                        });
                        updateData(index, hospitalData);
                    }
                    e.preventDefault();
                });
            }
            function deleteData(index) {
                $.ajax({
                    url: "/Handlers/HospitalHandler.ashx?id=" + index,
                    type: 'DELETE',
                    success: getData(),
                });
            }
            function updateData(index, hospitalData) {
                $.ajax({
                    url: "/Handlers/HospitalHandler.ashx?id=" + index,
                    type: 'PUT',
                    data: hospitalData,
                    success: getData(),
                });
            }
            function Options(index, selobj, nameattr1, nameattr2) {
                $(selobj).empty();
                $.getJSON('/Handlers/DoctorHandler.ashx', {}, function (data) {
                    $.each(data, function (i, obj) {
                        var FLname = [obj[nameattr1], obj[nameattr2]];
                        if (obj['DoctorID'] == (index + 1)) {
                            $(selobj).append(
                            $('<option selected></option>')
                                .val(FLname.join(' '))
                                .html(FLname.join(' ')));
                        } else {
                            $(selobj).append(
                            $('<option></option>')
                               .val(FLname.join(' '))
                               .html(FLname.join(' ')));
                        }
                    });
                });
            }

            function GetDoctorID(fullname, cb) {
                var items = [];
                var FLname = fullname.split(' ');
                $.ajax({
                    url: '/Handlers/DoctorHandler.ashx',
                    dataType: 'json',
                    async: false,
                    success: function (data) {
                        $.each(data, function (i, obj) {
                            items.push(obj);
                        });
                        var x = items.filter(function (val) {
                            return val.FirstName === FLname[0] && val.LastName === FLname[1];
                        });
                        cb(x[0].DoctorID);
                    }
                });
            }

            $(document).ready(function () {
                getData();
            });
        })();
    </script>
    </asp:Content>
