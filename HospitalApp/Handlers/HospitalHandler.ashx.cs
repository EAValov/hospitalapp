﻿using HospitalApp.Domain;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace HospitalApp.Handlers
{
    /// <summary>
    /// Сводное описание для HospitalHandler
    /// </summary>
    public class HospitalHandler : IHttpHandler
    {
        private Repository repo = new Repository();
        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.HttpMethod == "GET") 
            {
                context.Response.ContentType = "application/json";
                context.Response.ContentEncoding = Encoding.UTF8;

                int id;
                dynamic results;
                bool parsed = int.TryParse(context.Request.QueryString["id"], out id);
                if (parsed)
                {
                    results = JsonConvert.SerializeObject(repo.Hospitals.Where(y => y.HospitalID == id), Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                }
                else
                {
                    results = JsonConvert.SerializeObject(repo.Hospitals, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                }

                context.Response.Write(results);

            }
            else if (context.Request.HttpMethod == "POST") 
            {
                Hospital Hospital = new Hospital() { Name = context.Request.Form["Name"], DoctorID = int.Parse(context.Request.Form["DoctorID"]) }; // add doctor id
                repo.SaveHospital(Hospital);
            }
            else if (context.Request.HttpMethod == "PUT") 
            {
                int id;
                bool parsed = int.TryParse(context.Request.QueryString["id"], out id);
                if (parsed)
                {
                    Doctor doc = repo.Doctors.FirstOrDefault(y => y.DoctorID == (int.Parse(context.Request.Form["DoctorID"])));

                    Hospital newHospital = new Hospital()
                    {
                        HospitalID = int.Parse(context.Request.Form["HospitalID"]),
                        Name = context.Request.Form["Name"],
                        DoctorID = int.Parse(context.Request.Form["DoctorID"]),
                    };

                    if (doc != null)
                    {
                        newHospital.Doctor = new Doctor()
                        {
                            DoctorID = doc.DoctorID,
                            FirstName = doc.FirstName,
                            MiddleName = doc.MiddleName,
                            LastName = doc.LastName
                        };
                        repo.SaveDoctor(newHospital.Doctor);
                        repo.DeleteDoctor(doc.DoctorID);
                    }
                    repo.SaveHospital(newHospital);
                }

            }
            else if (context.Request.HttpMethod == "DELETE")
            {
                int id;
                bool parsed = int.TryParse(context.Request.QueryString["id"], out id);
                if (parsed)
                {
                    repo.DeleteHospital(id);
                }
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}