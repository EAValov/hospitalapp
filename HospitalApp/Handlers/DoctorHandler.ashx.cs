﻿using HospitalApp.Domain;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace HospitalApp.Handlers
{
    /// <summary>
    /// Сводное описание для DoctorHandler
    /// </summary>
    /// 
    public class DoctorHandler : IHttpHandler
    {

        private Repository repo = new Repository();
        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.HttpMethod == "GET")
            {
                context.Response.ContentType = "application/json";
                context.Response.ContentEncoding = Encoding.UTF8;

                int id;
                int doc;
                dynamic results;
                bool parsedid = int.TryParse(context.Request.QueryString["id"], out id);
                bool parseddoc = int.TryParse(context.Request.QueryString["doc"], out doc);
                if (parseddoc)
                {
                    results = JsonConvert.SerializeObject(repo.GetHospitalsForDoctor(doc)); //TVF
                }
                else
                    if (parsedid)
                    {
                        results = JsonConvert.SerializeObject(repo.Doctors.Where(y => y.DoctorID == id), Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }
                    else
                    {
                        results = JsonConvert.SerializeObject(repo.Doctors, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    }

                context.Response.Write(results);

            }
            else if (context.Request.HttpMethod == "POST")
            {
                IEnumerable<Hospital> hospitals = repo.Hospitals.Where(t => t.HospitalID == int.Parse(context.Request.Form["HospitalID"])); //multyselectlist ready

                Doctor Doctor = new Doctor()
                {
                    FirstName = context.Request.Form["FirstName"],
                    MiddleName = context.Request.Form["MiddleName"],
                    LastName = context.Request.Form["LastName"],
                };

                foreach (var t in hospitals)
                {
                    Hospital newhosp = new Hospital()
                    {
                        HospitalID = t.HospitalID,
                        Name = t.Name,
                        DoctorID = Doctor.DoctorID
                    };
                    Doctor.Hospitals.Add(newhosp);
                }

                repo.SaveDoctor(Doctor);
            }
            else if (context.Request.HttpMethod == "PUT")
            {
                int id;
                bool parsed = int.TryParse(context.Request.QueryString["id"], out id);
                if (parsed)
                {
                    JArray hospitals = JArray.Parse(context.Request.Form["HospitalArray[Hospitals]"]);

                    Doctor Doctor = new Doctor()
                    {
                        DoctorID = int.Parse(context.Request.Form["DoctorID"]),
                        FirstName = context.Request.Form["FirstName"],
                        MiddleName = context.Request.Form["MiddleName"],
                        LastName = context.Request.Form["LastName"],
                    };

                    foreach (JObject t in hospitals.Children<JObject>())
                    {
                        Hospital newhosp = new Hospital()
                        {
                            HospitalID = int.Parse(t["HospitalID"].ToString()),
                            Name = t["Name"].ToString(),
                            DoctorID = Doctor.DoctorID
                        };
                        Doctor.Hospitals.Add(newhosp);

                    }

                    repo.SaveDoctor(Doctor);
                }

            }
            else if (context.Request.HttpMethod == "DELETE")
            {
                int id;
                bool parsed = int.TryParse(context.Request.QueryString["id"], out id);
                if (parsed)
                {
                    repo.DeleteDoctor(id);
                }
            }

        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}