﻿CREATE TABLE [dbo].[Doctor] (
    [DoctorID]       INT IDENTITY (1, 1) NOT NULL,
    [FirstName]      NVARCHAR (100) NULL,
    [MiddleName]     NVARCHAR (100) NULL,
    [LastName]       NVARCHAR (100) NULL,
    
    PRIMARY KEY CLUSTERED ([DoctorID] ASC),
);

CREATE TABLE [dbo].[Hospital] (
    [HospitalID]     INT IDENTITY (1, 1) NOT NULL,
    [Name]           NVARCHAR (100) NULL,
    [DoctorID]       INT NOT NULL,

	CONSTRAINT [FK_dbo.DoctorID] FOREIGN KEY ([DoctorID]) REFERENCES [dbo].[Doctor] ([DoctorID]) ON DELETE CASCADE,
    PRIMARY KEY CLUSTERED ([HospitalID] ASC),
    );

GO

CREATE FUNCTION [dbo].[GetHospitalsForDoctor] (@DoctorID INT) RETURNS TABLE 
 
RETURN 
    SELECT [HospitalID],
	       [Name]   

    FROM   [dbo].[Hospital] 
    WHERE  DoctorID = @DoctorID

go

INSERT INTO Doctor(FirstName, MiddleName, LastName) VALUES ( 'Perry', 'Ulysses', 'Cox' );
INSERT INTO Doctor(FirstName, LastName) VALUES ( 'Gregory', 'House' );
INSERT INTO Doctor(FirstName, MiddleName, LastName) VALUES ( 'Jonh', 'Michael', 'Dorian' );

go

INSERT INTO Hospital(Name, DoctorID) VALUES('Sacred Heart',  (SELECT DoctorID from Doctor WHERE FirstName ='Perry'));
INSERT INTO Hospital(Name, DoctorID) VALUES('Priston Plainsboro',  (SELECT DoctorID from Doctor WHERE FirstName ='Gregory'));
INSERT INTO Hospital(Name, DoctorID) VALUES('Hospital ER',  (SELECT DoctorID from Doctor WHERE FirstName ='Perry'));
INSERT INTO Hospital(Name, DoctorID) VALUES('My Test Hospital',  (SELECT DoctorID from Doctor WHERE FirstName ='Jonh'));