﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HospitalApp.Doctors
{
    public partial class Edit : System.Web.UI.Page
    {
        protected int id
        {
            get
            {
                int page;
                page = int.TryParse(Request.QueryString["id"], out page) ? page : 0;
                return page;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}