﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="List.aspx.cs" MasterPageFile="~/Layout.Master" %>

    <asp:Content runat="server" ContentPlaceHolderID ="head">   
        <script src="../Scripts/DoctorsList.js" type="text/javascript"></script>
    <script type="text/template" id="rowTemplate">
        <tr>
            <td>{DoctorID}</td>
            <td>{FirstName}</td>
            <td>{MiddleName}</td>
            <td>{LastName}</td>
            <td>{Hospitals} </td>
            <td>
                <button data-id="{DoctorID}" data-action="details">Edit</button>
            </td>
        </tr>
    </script>
        </asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="title">Doctors List</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID ="main">   
    <form id="form1" runat="server">
        <table id="dataTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>FirstName</th>
                    <th>MiddleName</th>
                    <th>LastName</th>
                    <th>Hospital</th>
                    <th></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </form>

    <button id="AddDoctorButton" class="float-left submit-button">Add New Doctor</button>

    <script type="text/javascript">
        document.getElementById("AddDoctorButton").onclick = function () {
            location.href = "../Doctors/Add.aspx";
        };
    </script>
        </asp:Content>

