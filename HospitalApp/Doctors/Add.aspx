﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" MasterPageFile="~/Layout.Master"%>
<asp:Content runat="server" ContentPlaceHolderID ="head"> 
    <script src="../Scripts/AddDoctor.js"  type="text/javascript"></script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="title">Add new Doctor</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID ="main">   
    <form id="addform" runat="server">
        <table id="dataTable">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <th>Last Name</th>
                    <th>Hospital</th>
                    <th></th>

                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><input type="text" name="FirstName" value="" /></td>
                    <td><input type="text" name="MiddleName" value="" /></td>
                    <td><input type="text" name="LastName" value="" /></td>
                    <td> <select id ="Hospital" name="Hospital"></select> </td>
                </tr>
            </tbody>
        </table>
        <div>
            <button id="AddNewDoctorButton" type="submit">Create new Doctor</button>
        </div>
    </form>

        <button id="CancelNewDoctor" class="float-left submit-button">Cancel and return to list</button>

    <script type="text/javascript">
        document.getElementById("CancelNewDoctor").onclick = function () {
            location.href = "../Doctors/List.aspx";
        };
    </script>
</asp:Content>
