﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="HospitalApp.Doctors.Edit" MasterPageFile="~/Layout.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="head">
    <script type="text/template" id="rowTemplate">
        <tr>
            <td>{DoctorID}</td>
            <td>
                <input type="text" name="FirstName" value="{FirstName}" />
            </td>
            <td>
                <input type="text" name="MiddleName" value="{MiddleName}" />
            </td>
            <td>
                <input type="text" name="LastName" value="{LastName}" />
            </td>
            <td>
                <button data-id="{DoctorID}" data-action="update">Update</button>
                <button data-id="{DoctorID}" data-action="delete">Delete</button>
            </td>
        </tr>
    </script>

    <script type="text/template" id="HospitalsTemplate">
        <tr class="myHospitals">
            <td><span class="HospitalID">{HospitalID}</span></td>
            <td><span class="Name">{Name}</span></td>
            <td>
                <button data-id="{DoctorID}" data-action="delete">Delete</button></td>
        </tr>
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="title">Doctor Edit</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="main">
    <form id="form1" runat="server">
      <h2>Doctor</h2>
        <table id="dataTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>FirstName</th>
                    <th>MiddleName</th>
                    <th>LastName</th>
                    <th></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
        <hr />
         <h2>Hospitals</h2>
        <table id="HospitalTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th></th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>

    </form>

    <button id="CancelNewDoctor" class="float-left submit-button">Cancel and return to list</button>

    <script type="text/javascript">
        document.getElementById("CancelNewDoctor").onclick = function () {
            location.href = "../Doctors/List.aspx";
        };
    </script>

    <script type="text/javascript">
        (function () {
            String.prototype.format = function (dataObject) {
                return this.replace(/{(.+)}/g, function (match, propertyName) {
                    return dataObject[propertyName];
                });
            };
            function getData() {
                var myID = '<%=id%>'; //this is bad?
                if (myID != 0) {
                    $.getJSON("/Handlers/DoctorHandler.ashx?id=" + myID, null, displayData);
                } else { location.href = "../Doctors/List.aspx"; }
                $.getJSON("/Handlers/DoctorHandler.ashx?doc=" + myID, null, fillHospitals);
            };
            function fillHospitals(data) {
                var target = $("#HospitalTable tbody");
                target.empty();
                var template = $("#HospitalsTemplate");
                data.forEach(function (dataObject) {
                    target.append(template.html().format(dataObject));
                });
                $(target).find("button").click(function (e) {
                    if ($(e.target).attr("data-action") == "delete") {
                        var tr = $(this).closest('tr');
                        tr.css("background-color", "#FF3700");
                        tr.fadeOut(400, function () {
                            tr.remove();
                        });
                        return false;
                    }
                    e.preventDefault();
                });
            }

            function displayData(data) {
                var target = $("#dataTable tbody");
                target.empty();
                var template = $("#rowTemplate");
                data.forEach(function (dataObject) {
                    target.append(template.html().format(dataObject));
                });
                $(target).find("button").click(function (e) {

                    var index = $(e.target).attr("data-id");
                    if ($(e.target).attr("data-action") == "delete") {
                        deleteData(index);
                    } else {
                        var doctorData = { DoctorID: index };

                        $(e.target).closest('tr').find('input')
                        .each(function (index, inputElem) {
                            doctorData[inputElem.name] = inputElem.value;
                        });
                        var Hospitalarray = [];
                        var MyHospitals = $(".myHospitals");
                        MyHospitals.each(function (index, Hosp) {
                            var hospitalData = { DoctorID: doctorData["DoctorID"] };
                            $(Hosp).find('span').each(function (index, Elem) {
                                var cl = $(Elem).attr("class");
                                var txt = $(Elem).text();
                                hospitalData[cl] = txt;
                            });
                            Hospitalarray.push(hospitalData);
                        })
                        doctorData["HospitalArray"] = { Hospitals: JSON.stringify(Hospitalarray) };
                        updateData(index, doctorData);
                    }
                    e.preventDefault();
                });

            }

            function deleteData(index) {
                $.ajax({
                    url: "/Handlers/DoctorHandler.ashx?id=" + index,
                    type: 'DELETE',
                    success: getData,
                    error: alert("Error - Method DELETE not allowed?"),
                });
            }
            function updateData(index, doctorData) {
                $.ajax({
                    url: "/Handlers/DoctorHandler.ashx?id=" + index,
                    type: 'PUT',
                    data: doctorData,
                    success: getData,
                });
            }
            $(document).ready(function () {
                getData();
            });
        })();
    </script>
</asp:Content>
