﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace HospitalApp.Domain
{
    public class Repository 
    {
        private HospitalDBEntities context = new HospitalDBEntities();
        //private MyContext context = new MyContext();
        public IEnumerable<Hospital> Hospitals { get { return context.Hospitals; } }
        public IEnumerable<Doctor> Doctors { get { return context.Doctors; } }

        public IEnumerable<GetHospitalsForDoctor_Result> GetHospitalsForDoctor(int doctorID)
        {
            var result = from r in context.GetHospitalsForDoctor(doctorID) select r;
            return result;
        }

        public void SaveHospital(Hospital hospital) 
        {

            if (hospital.HospitalID == 0)
            {
                context.Hospitals.Add(hospital);
            }
            else
            {
                Hospital dbEntry = context.Hospitals.Find(hospital.HospitalID);
                if (dbEntry == hospital)
                {
                    context.Entry(dbEntry).State = System.Data.Entity.EntityState.Modified;
                }
                else

                if (dbEntry != null)
                {
                    context.Hospitals.Remove(dbEntry);
                    context.Hospitals.Add(hospital);
                }
            }
                context.SaveChanges();
          
        }

        public void SaveDoctor(Doctor doctor)
        {

            if (doctor.DoctorID == 0)
            {
                context.Doctors.Add(doctor);
            }
            else
            {
                Doctor dbEntry = context.Doctors.Find(doctor.DoctorID);
                if (doctor.Hospitals.Count == 0)
                {
                    context.Entry(dbEntry).State = System.Data.Entity.EntityState.Modified;
                }
                else
                if (dbEntry != null)
                {
                    context.Doctors.Remove(dbEntry);
                    context.Doctors.Add(doctor);
                }
            }
            context.SaveChanges();
        }
        public Hospital DeleteHospital(int HospitalID)
        {
            Hospital dbEntry = context.Hospitals.Find(HospitalID);
            if (dbEntry != null)
            {
                context.Hospitals.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }

        public Doctor DeleteDoctor(int DoctorID)
        {
            Doctor dbEntry = context.Doctors.Find(DoctorID);
            if (dbEntry != null)
            {
                context.Doctors.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }
    }
}

