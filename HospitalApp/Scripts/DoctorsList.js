﻿(function () {
    String.prototype.format = function (dataObject) {
        return this.replace(/{(.+)}/g, function (match, propertyName) {
            return dataObject[propertyName];
        });
    };
    function getData() {
        $.getJSON("/Handlers/DoctorHandler.ashx", null, displayData);
    };
    function displayData(data) {
        var target = $("#dataTable tbody");
        target.empty();
        var template = $("#rowTemplate");
        data.forEach(function (dataObject) {
            var l1 = dataObject["Hospitals"].length;
            dataObject["Hospitals"] = l1;
            target.append(template.html().format(dataObject));
        });
        $('select#Hospital').each(function (index, value) {
            Options(index, value, 'Name');
        });
        $(target).find("button").click(function (e) {
            $("*.errorMsg").remove();
            $("*.error").removeClass("error");
            var index = $(e.target).attr("data-id");
            if ($(e.target).attr("data-action") == "details") {
                detailView(index);
            }
            e.preventDefault();
        });
    }
    function Options(index, selobj, nameattr) {
        $(selobj).empty();
        $.getJSON('/Handlers/HospitalHandler.ashx', {}, function (data) {
            $.each(data, function (i, obj) {
                if (obj['HospitalID'] == (index + 1)) {
                    $(selobj).append(
                    $('<option selected></option>')
                           .val(obj[nameattr])
                           .html(obj[nameattr]));
                } else {
                    $(selobj).append(
                    $('<option></option>')
                           .val(obj[nameattr])
                           .html(obj[nameattr]));
                }
            });
        });
    }

    function detailView(index) {
        document.location = "Edit.aspx?id=" + index;
    }

    function GetHospitalID(hospitalname, cb) {
        var items = [];
        $.ajax({
            url: '/Handlers/HospitalHandler.ashx',
            dataType: 'json',
            async: false,
            success: function (data) {
                $.each(data, function (i, obj) {
                    items.push(obj);
                });
                var x = items.filter(function (val) {
                    return val.Name === hospitalname;
                });
                cb(x[0].HospitalID);
            }
        });
    }

    $(document).ready(function () {
        getData();
    });
})();