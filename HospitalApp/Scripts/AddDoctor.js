﻿$(document).ready(function () {
    var target = $("#addform");

    $('select#Hospital').each(function (index, value) {
        Options(index, value, 'Name');
    });

    $("#AddNewDoctorButton").click(function () {
        $("*.errorMsg").remove();
        $("*.error").removeClass("error");
        var DoctorData = {};
        var Hospital = $('#Hospital').find(":selected").text();
        $(target).find('input')
        .each(function (index, inputElem) {
            DoctorData[inputElem.name] = inputElem.value;
        });
        GetHospitalID(Hospital, function (name) {
            DoctorData['HospitalID'] = name;
        });
        SaveDoctor(DoctorData);
    });

    function Options(index, selobj, nameattr) {
        $(selobj).empty();
        $.getJSON('/Handlers/HospitalHandler.ashx', {}, function (data) {
            $.each(data, function (i, obj) {
                $(selobj).append(
                $('<option></option>')
                       .val(obj[nameattr])
                       .html(obj[nameattr]));
            });
        });
    }

    function GetHospitalID(hospitalname, cb) {
        var items = [];
        $.ajax({
            url: '/Handlers/HospitalHandler.ashx',
            dataType: 'json',
            async: false,
            success: function (data) {
                $.each(data, function (i, obj) {
                    items.push(obj);
                });
                var x = items.filter(function (val) {
                    return val.Name === hospitalname;
                });
                cb(x[0].HospitalID);
            }
        });
    }

    function SaveDoctor(DoctorData) {
        $.ajax({
            url: "/Handlers/DoctorHandler.ashx",
            type: 'POST',
            data: DoctorData,
            success: location.reload(),
        });
    }
});