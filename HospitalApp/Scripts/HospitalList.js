﻿        (function () {
            String.prototype.format = function (dataObject) {
                return this.replace(/{(.+)}/g, function (match, propertyName) {
                    return dataObject[propertyName];
                });
            };
            function getData() {
                $.getJSON("/Handlers/HospitalHandler.ashx?", null, displayData);
            };
            function displayData(data) {
                var target = $("#dataTable tbody");
                target.empty();
                var template = $("#rowTemplate");
                data.forEach(function (dataObject) {
                    var l1 = dataObject["Doctor"].FirstName + ' ' + dataObject["Doctor"].LastName;
                    dataObject["Doctor"] = l1;
                    target.append(template.html().format(dataObject));
                });
                $(target).find("button").click(function (e) {
                    $("*.errorMsg").remove();
                    $("*.error").removeClass("error");
                    var index = $(e.target).attr("data-id");
                    if ($(e.target).attr("data-action") == "details") {
                        detailView(index);
                    }
                    e.preventDefault();
                });
            }

            function detailView(index) {
                location.href = "Edit.aspx?id=" + index;
            }

            $(document).ready(function () {
                getData();
            });
        })();