﻿$(document).ready(function () {
    var target = $("#addform");

    $('select#Doctor').each(function (index, value) {
        Options(index, value, 'FirstName', 'LastName');
    });

    $("#AddNewHospitalButton").click(function () {
        $("*.errorMsg").remove();
        $("*.error").removeClass("error");
        var DoctorName = $('#Doctor').find(":selected").text();
        var hospitalData = {};
        $(target).find('input')
        .each(function (index, inputElem) {
            hospitalData[inputElem.name] = inputElem.value;
        });

        GetDoctorID(DoctorName, function (name) {
            hospitalData['DoctorID'] = name;
        });
        SaveHospital(hospitalData);
    });

    function Options(index, selobj, nameattr1, nameattr2) {
        $(selobj).empty();
        $.getJSON('/Handlers/DoctorHandler.ashx', {}, function (data) {
            $.each(data, function (i, obj) {
                var FLname = [obj[nameattr1], obj[nameattr2]];
                $(selobj).append(
                $('<option></option>')
                       .val(FLname.join(' '))
                       .html(FLname.join(' ')));
            });
        });
    }

    function GetDoctorID(fullname, cb) {
        var items = [];
        var FLname = fullname.split(' ');
        $.ajax({
            url: '/Handlers/DoctorHandler.ashx',
            dataType: 'json',
            async: false,
            success: function (data) {
                $.each(data, function (i, obj) {
                    items.push(obj);
                });
                var x = items.filter(function (val) {
                    return val.FirstName === FLname[0] && val.LastName === FLname[1];
                });
                cb(x[0].DoctorID);
            }
        });
    }

    function SaveHospital(hospitalData) {
        $.ajax({
            url: "/Handlers/HospitalHandler.ashx",
            type: 'POST',
            data: hospitalData,
            success: function () { location.href = "../Hospitals/List.aspx" }
        });
    }
});